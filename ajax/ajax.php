<? 
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	$action = trim($_REQUEST["action"]);

	# Сканер безопасности битрикса попросил сделать кейсом ...
	switch ($action) {
		case "getContentMap":	    $file = "getContentMap.php"; break;
		default: 					die("Действие не доступно"); break;
	}

	if ( !file_exists($file) ) {
		die("Действие не доступно");
	} else {
		require_once($file);
	}
?>