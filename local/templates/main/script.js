jQuery(function ($) {
    /*#10737*/
    /*Если мы находимся на странице с картой, вызываем функцию отображения карты*/
    if ($('.js-yandex-filtrable-map').length > 0) {
        initYandexMap();
    }

    function initYandexMap() {
        /*переменные*/
        var arContentMap = {},           // массив с результатами выборки по фильтру
            myMap,                   // карта
            $eventSelect = $(".js-example-events"),
            idTickets = '',
            arTickets = {},
            idDeleteTickets = '',
            idMetro,
            typePoint,
            idViewPoint,
            idTypePoint;

        $eventSelect.on("select2:select", function (e) {
            setContentMap("select2:select", e);
        });
        $eventSelect.on("select2:unselect", function (e) {
            setContentMap("select2:unselect", e);
        });
        /*получем значения из фильтра и делаем запрос*/
        function setContentMap(name, evt, id_metro) {
            //idMetro = id_metro;
            arContentMap = {};
            /*если удаляем тип билета из селекта, то удаляем его и из массива*/
            if (evt.type == 'select2:unselect') {
                JSON.stringify(evt.params, function (key, value) {
                    idDeleteTickets = evt.params['data']['element']['attributes']['1']['nodeValue'];
                    delete arTickets[idDeleteTickets];
                });
            }
            /*если добавляем в селект, то добавляем в массив*/
            else if (evt.type == 'select2:select') {
                JSON.stringify(evt.params, function (key, value) {
                    /*в ['data']['element']['attributes'] те атрибуты, которые указаны у <option> в template.php компонента и порядок такой же*/
                    idTickets = evt.params['data']['element']['attributes']['1']['nodeValue'];

                    arTickets[idTickets] = idTickets;
                });
            }
            $.get('/ajax/ajax.php', {
                action: 'getContentMap',
                idMetro: idMetro,
                arIdTickets: arTickets
            }, function (data) {
                $json = $.parseJSON(data);
                for (pointID in $json) {
                    if ($json[pointID].hasOwnProperty('COORDINATES_ON_MAP')) {
                        $json[pointID]['COORDINATES_ON_MAP'] = $json[pointID]['COORDINATES_ON_MAP'].split(',');
                    }

                    /*Если есть свойство TYPE_TICKET, то получаем кол-во его элементов*/
                    if ($json[pointID].hasOwnProperty('TYPE_TICKET')) {
                        lengthTypeTicket = $json[pointID]['TYPE_TICKET'].length;
                    }

                    /*если есть свойство TICKETS или кол-во элементов у свойства TYPE_TICKET  = 0, то ставим точки на карту*/
                    if ($json[pointID].hasOwnProperty('TICKETS') || (lengthTypeTicket == 0 )) {
                        arContentMap[pointID] = $json[pointID];

                        idTypePoint = $json[pointID]['TYPE_POINT_ENUM_ID'];

                        idViewPoint = $json[pointID]['VIEW_POINT_ENUM_ID'];

                        if (idViewPoint && $json[pointID]['COLOR']) {
                            arContentMap[pointID]['VIEW_POINT_CODE'] = $json['VIEW_POINT'][arContentMap[pointID]['VIEW_POINT_ENUM_ID']];
                        }
                        else if (idTypePoint && !idViewPoint) {
                            typePoint = $json['TYPE_POINT_XML_ID'][arContentMap[pointID]['TYPE_POINT_ENUM_ID']];

                            if (typePoint == getLangMessage('CLIENT_OFFICE')) {
                                arContentMap[pointID]['VIEW_POINT_CODE'] = 'islands#dotIcon';
                                arContentMap[pointID]['COLOR'] = '0091ff';
                            }
                            else if (typePoint == getLangMessage('STAD_SHOP_TERM')) {
                                arContentMap[pointID]['VIEW_POINT_CODE'] = 'islands#circleIcon';
                                arContentMap[pointID]['COLOR'] = '0000ff';
                            }
                            else if (typePoint == getLangMessage('AGENTS')) {
                                arContentMap[pointID]['VIEW_POINT_CODE'] = 'islands#circleIcon';
                                arContentMap[pointID]['COLOR'] = '8000FF';
                            }
                        }
                        else if (!idTypePoint && !idViewPoint) {
                            arContentMap[pointID]['VIEW_POINT_CODE'] = 'islands#icon';
                            arContentMap[pointID]['COLOR'] = '0000ff';
                        }
                    }
                }

                if (myMap) {
                    myMap.destroy();
                }
                generateYandexMaps(arContentMap);
            });
        }

        /*при выборе станции метро передаем ф-ии id выбранного метро */
        $('body').on('click', '.cusel-scroll-pane span', function () {
            idMetro = $(this).attr('data-id-metro');
            setContentMap('idMetro', 'select_metro', idMetro);
        });

        setContentMap('', '', '');
        ymaps.ready(generateYandexMaps);

        /*#10737 Создаем Яндекс карту на странице Места продажи*/
        /*инициализация карты*/
        function generateYandexMaps() {
            var myGeoObjects = new ymaps.GeoObjectCollection({}, {
                preset: "islands#redCircleIcon",
                strokeWidth: 4,
                geodesic: true
            });

            myMap = new ymaps.Map("map",
                {
                    /*центрирование карты на г. Санкт - Петербург - Площадь Восстания*/
                    center: [59.93114403, 30.36065984],
                    /*элементы управления*/
                    controls: ['zoomControl', 'fullscreenControl'],
                    zoom: 14
                },
                {
                    maxZoom: 18,
                    minZoom: 1
                });
            /*проходим по массиву с точками и вытаскиваем данные*/
            for (idPoint in arContentMap) {
                var contentHeader = '<div class = "contentHeader">',
                    contentBody = '<div class = "contentBody">', contentFooter;

                contentHeader = contentHeader + '<div class = "icon" style="background: url(' + arContentMap[idPoint]['LOGO'] + ') no-repeat"></div><div class = "NameOrg">' + arContentMap[idPoint]['NAME_POINT'] + '</div></div>';

                /*проверим, указаны ли типы билетов, если да, то выводим их*/
                if (arContentMap[idPoint]['TICKETS']) {
                    contentFooter = '<div class = "contentFooter"> Типы билетов: ';

                    for (arSect in arContentMap[idPoint]['TICKETS']) {
                        if (arContentMap[idPoint]['TICKETS'][arSect]) {
                            contentFooter = contentFooter + arSect + ': ' + arContentMap[idPoint]['TICKETS'][arSect] + '; ';
                        }
                    }
                    contentFooter = contentFooter + '</div>';
                }
                else {
                    contentFooter = '';
                }

                /*проверяем заполненность выводимых полей в балуне*/
                if (arContentMap[idPoint]['METRO']) {
                    contentBody = contentBody + getLangMessage('METRO') + arContentMap[idPoint]['METRO'] + '<br>';
                }

                if (arContentMap[idPoint]['DISTRICT']) {
                    contentBody = contentBody + getLangMessage('DISTRICT') + arContentMap[idPoint]['DISTRICT'] + '<br>';
                }

                if (arContentMap[idPoint]['ADDRESS']) {
                    contentBody = contentBody + getLangMessage('ADDRESS') + arContentMap[idPoint]['ADDRESS'] + '<br>';
                }

                if (arContentMap[idPoint]['WORK_HOURS']) {
                    contentBody = contentBody + getLangMessage('WORK_HOURS') + arContentMap[idPoint]['WORK_HOURS'] + '<br>';
                }

                if (arContentMap[idPoint]['PHONE']) {
                    contentBody = contentBody + getLangMessage('PHONE') + arContentMap[idPoint]['PHONE'] + '<br>';
                }

                if (arContentMap[idPoint].hasOwnProperty('COORDINATES_ON_MAP')) {
                    /*добавление метки*/
                    myGeoObjects.add(new ymaps.Placemark([
                            arContentMap[idPoint]['COORDINATES_ON_MAP']['0'],
                            arContentMap[idPoint]['COORDINATES_ON_MAP']['1']
                        ],
                        {
                            /*Заголовок*/
                            balloonContentHeader: contentHeader,
                            /*основной контент*/
                            balloonContentBody: contentBody,
                            balloonContentFooter: contentFooter
                        },
                        {
                            preset: arContentMap[idPoint]['VIEW_POINT_CODE'],
                            iconColor: '#' + arContentMap[idPoint]['COLOR']
                        }));

                    /*добавляем метку на карту*/
                    myMap.geoObjects.add(myGeoObjects);
                    myMap.setBounds(myGeoObjects.getBounds());
                }
            }
        }
    }
});