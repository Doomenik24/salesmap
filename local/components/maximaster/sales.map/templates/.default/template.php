<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class = "point_of_sale_filter">
    <span class="search_station"><?=GetMessage('SEARCH_BY_ST_METRO');?></span>
    <select class="cuseled">
        <option value=""></option>
        <?foreach($arResult['METRO_TO_SELECT'] as $idMetro => $sNameMetro):?>
            <?if($idMetro):0?>
                <option value="<?=$sNameMetro;?>" data-id-metro="<?=$idMetro?>"> <?=$sNameMetro;?> </option>
            <?endif;?>
        <?endforeach;?>
    </select>
    <br>
    <br>
    <span class="search_type_tickets"><?=GetMessage('SEARCH_BY_TYPE_TICKETS');?></span>
    <select class="js-example-basic-multiple js-example-events" multiple="multiple">
        <?foreach($arResult['TICKETS_TO_SELECT'] as $sNameSection => $arTickets):?>
            <optgroup label="<?=$sNameSection?>">
                <?foreach($arTickets as $idTickets => $sNameTickets):?>
                    <option value="<?=$sNameTickets;?>" data-id-tickets="<?=$idTickets?>"> <?=$sNameTickets;?> </option>
                <?endforeach;?>
            </optgroup>
        <?endforeach;?>
    </select>
</div>
<div id="map" class="yandex-filtrable-map js-yandex-filtrable-map"></div>

<script type="text/javascript">
    $('select.js-example-basic-multiple').select2();
</script>