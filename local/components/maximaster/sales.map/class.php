<?php
namespace Mx\Main\Component;

class SalesMap extends Base
{
    public function executeComponent()
    {
        $cacheTime = 3600;
        if($this->startResultCache($cacheTime))
        {
            $contentMap = self::getContentMap();
            if(!$contentMap)
            {
                $this->abortResultCache();
                return;
            }

            if($_REQUEST['action'] == 'getContentMap')
            {
                echo json_encode($contentMap);
                die();
            }

            $this->arResult = $contentMap;

            $this->IncludeComponentTemplate();
        }
    }

    public static function getContentMap()
    {
        $idMetro = $_REQUEST['idMetro'] ;
        $arIdSelectTickets = $_REQUEST['arIdTickets'];
        $arFilterMetro = Array("IBLOCK_ID"=> IBLOCK_ID_POINT_OF_SALE, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");

        if ($idMetro) $arFilterMetro['PROPERTY_METRO'] = array($idMetro);
        $arTickets = array(); //массив типов билетов
        $arIdSectionTickets = array();
        $arSelect = Array(
            'ID',
            'PROPERTY_NAME_POINT',
            'PROPERTY_LOGO',
            'PROPERTY_METRO',
            'PROPERTY_ADDRESS',
            'PROPERTY_WORK_HOURS',
            'PROPERTY_PHONE',
            'PROPERTY_TYPE_TICKET',
            'PROPERTY_COORDINATES_ON_MAP',
            'PROPERTY_COLOR',
            'PROPERTY_VIEW_POINT',
            'PROPERTY_TYPE_POINT',
            'PROPERTY_DISTRICT'
        );
        $res = \CIBlockElement::GetList(Array(), $arFilterMetro, false, false, $arSelect);
        $arPointList = array();
        $arIdViewPoint = array();
        while($arPoint = $res->Fetch())
        {
            $pointId = $arPoint['ID'];
            foreach($arPoint['PROPERTY_TYPE_TICKET_VALUE'] as $key => $idTickets)
            {
                $arTickets[$pointId][$idTickets] = $idTickets;
            }

            /*записываем в массив id вида точки*/
            $arIdViewPoint[$pointId] = $arPoint['PROPERTY_VIEW_POINT_ENUM_ID'];

            foreach ($arPoint as $key => $value)
            {
                $key = str_replace(array('PROPERTY_', '_VALUE'), '', $key);
                if ($key == 'LOGO')
                {
                    $arPic = \CFile::ResizeImageGet($value, array("width" => 60, "height" => 60), BX_RESIZE_IMAGE_PROPORTIONAL);
                    $value = $arPic['src'];
                }
                $arPointList[ $pointId ][ $key ] = $value;

                if($key == 'METRO')
                {
                    $arMetro[$arPoint['PROPERTY_METRO_ENUM_ID']] = $value;
                }
            }
        }

        /*получаем XML_ID значений  свойства "Вид точки на карте"*/
        $arViewPoint = \CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>IBLOCK_ID_POINT_OF_SALE, "CODE" => "VIEW_POINT"));

        while($arItem = $arViewPoint->Fetch())
        {
            $arPointList['VIEW_POINT'][$arItem['ID']] = $arItem['XML_ID'];
        }

        /*получаем XML_ID значений  свойства "Тип точки"*/
        $arViewPoint = \CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>IBLOCK_ID_POINT_OF_SALE, "CODE" => "TYPE_POINT"));

        while($arItem = $arViewPoint->Fetch())
        {
            $arPointList['TYPE_POINT_XML_ID'][$arItem['ID']] = $arItem['XML_ID'];
        }

        //получаем имена разделов
        $arNameSection = array();
        $db_list = \CIBlockSection::GetList(Array(), array('IBLOCK_ID'=>IBLOCK_ID_TYPE_TICKET, 'CHECK_PERMISSIONS' => 'N'), false, array('ID','NAME'));
        while($res = $db_list->Fetch())
        {
            $arNameSection[$res['ID']] = $res['NAME'];
        }

        //получаем типы билетов
        $arFilterTickets = Array(
            "IBLOCK_ID" => IBLOCK_ID_TYPE_TICKET,
            'ID' => $arTickets,
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y"
        );
        if ($arIdSelectTickets) {
            $arFilterTickets['ID'] = $arIdSelectTickets;
        }
        $arSelect = Array('NAME', 'IBLOCK_SECTION_ID','ID');
        $res = \CIBlockElement::GetList(Array(), $arFilterTickets, false, false, $arSelect);
        while($arProperties = $res->Fetch())
        {
            foreach($arTickets as $IdPoint => $arIdTickets )
            {
                if($arIdTickets[$arProperties['ID']])
                {
                    $sNameKindSport = $arNameSection[$arProperties['IBLOCK_SECTION_ID']]; //название вида спорта

                    $sTicketsId = $arProperties['ID']; //id билета

                    $arPointList[$IdPoint]['TICKETS'][$sNameKindSport][$sTicketsId] = $arProperties['NAME'];

                    $arPointList['TICKETS_TO_SELECT'][$sNameKindSport][$sTicketsId]= $arProperties['NAME'];
                }

            }
            $arIdSectionTickets[$arProperties['IBLOCK_SECTION_ID']] = $arProperties['IBLOCK_SECTION_ID'];
        }
        //разбиваем массив с типами билетов по запятой, чтобы корректно вывести их у балуна на карте
        foreach($arPointList as $idPoint => $arTickets)
        {
            if($arTickets['TICKETS'])
            {
                foreach($arTickets['TICKETS'] as $sNameSection => $arPropTickets)
                {
                    $arPointList[$idPoint]['TICKETS'][$sNameSection] = implode(', ', $arPropTickets);
                }
            }
        }
        $arPointList['METRO_TO_SELECT'] = $arMetro;
        return $arPointList;
    }
}